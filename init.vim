call plug#begin('~/nvim/plugged')
Plug 'tpope/vim-fugitive'
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
Plug 'Shougo/deoplete.nvim'
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'
endif
Plug 'StanAngeloff/php.vim'
Plug 'junegunn/fzf.vim'
Plug 'ludovicchabant/vim-gutentags'
Plug 'scrooloose/nerdtree'
Plug 'itchyny/lightline.vim'
Plug 'majutsushi/tagbar'
Plug 'phpactor/phpactor'
Plug 'autozimu/LanguageClient-neovim', { 'do': ':UpdateRemotePlugins' }
Plug 'roxma/LanguageServer-php-neovim',  {'do': 'composer install && composer run-script parse-stubs', 'for': 'php'}
Plug 'joonty/vdebug'
Plug 'adoy/vim-php-refactoring-toolbox', {'for': 'php'}
Plug 'tobyS/pdv'
Plug 'donRaphaco/neotex', { 'for': 'tex' }
call plug#end()

let g:deoplete#enable_at_startup = 1

" <Leader> is "\"... but on azerty keyboard it better to use "," wich is more accessible
let mapleader = ","

" line & relativenumber
set number
set relativenumber

" tab settings
set expandtab
set shiftwidth=2
set softtabstop=2

" Right Tagbar
nmap <F8> :TagbarToggle<CR>

" Nerd Tree Toggle
nmap <F12> :NERDTreeToggle<CR>

" Php Actor
" Invoke the context menu
nmap <Leader>mm :call phpactor#ContextMenu()<CR>

" Goto definition of class or class member under the cursor
nmap <Leader>o :call phpactor#GotoDefinition()<CR>

" Fzf 
nmap <Leader>f :Files<CR>

" LSP server
" only start lsp when opening php files
au filetype php LanguageClientStart

" I only use these 3 mappings

nnoremap <silent> gr :call LanguageClient_textDocument_references()<CR>
nnoremap <silent> gd :call LanguageClient_textDocument_definition()<CR>
nnoremap K :call LanguageClient_textDocument_hover()<cr>

" Rip Grep
nnoremap <leader>a :Rg<space>
nnoremap <leader>A :exec "Rg ".expand("<cword>")<cr>

autocmd VimEnter * command! -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --glob "!.git/* !tags" --color "always" '.shellescape(<q-args>), 1,
  \   <bang>0 ? fzf#vim#with_preview('up:60%')
  \           : fzf#vim#with_preview('right:80%:hidden', '?'),
  \   <bang>0)


